import aiohttp
import asyncio



async def main():
    url = 'https://us-central1-paas-e9dd6.cloudfunctions.net/generateCheckout'
    headers = {'Content-Type': 'application/json', 'Paas-Api-Key':'iePllpgFXTkJ267U7Acr'}


    json = {
        'email': 'miguel@gmail.com',
        'lineItems': [{'ean': 10004, 'quantity': 1}],
        'address': [{
            'address1': 'cdmx',
            'address2': 'cdmx',
            'city': 'cdmx',
            "province": 'cdmx',
            'zip': '1234',
            'firstName': 'digital',
            'lastName': 'class',
            'phone': '5541258769'
        }]
    }


    async with aiohttp.ClientSession() as session:
        async with session.post(url, json=json, headers=headers) as resp:
            #print(resp.status)
            #print(await resp.json())
            response = await resp.json()
            print('url: ', response['url'])
            print('token: ', response['token'])
            print('total: ', response['total'])
            print('state: ', response['state'])
            print('email: ', response['email'])




loop = asyncio.get_event_loop()
loop.run_until_complete(main())


